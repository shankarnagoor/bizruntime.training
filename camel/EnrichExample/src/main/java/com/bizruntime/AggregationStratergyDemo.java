package com.bizruntime;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class AggregationStratergyDemo implements AggregationStrategy
{

	@Override
	public Exchange aggregate(Exchange original, Exchange resource) {
		String originalBody=original.getIn().getBody(String.class);
		String resourceBody=original.getIn().getBody(String.class);
		String merge=originalBody+""+resourceBody;
		if(original.getPattern().isOutCapable())
		{
			original.getOut().setBody(merge);
		}
		else
		{
			original.getIn().setBody(merge);
		}
		return original;
	   /* String old = resource.getIn().getBody(String.class);
	    System.out.println("OLD:: \n"+old);
	    String newMsg = original.getIn().getBody(String.class);
	    System.out.println("NEW:: \n"+newMsg);
	    System.out.println("MERGED::" + old + newMsg);
	    original.getIn().setBody(old+newMsg);
	     return original;*/
	}

}
