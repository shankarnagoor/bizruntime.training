package com.bizruntime;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import com.bizruntime.POJO1.Customer;

public class LookupCustomerProcessor implements Processor{

	Logger log = Logger.getLogger(LookupCustomerProcessor.class);
	@Override
	public void process(Exchange exchange) throws Exception {
		Object[] obj = exchange.getIn().getBody(Object[].class);	
		String id = (String) obj[0];
		
		log.debug("Getting detail for customer '" + id + "'");
		
		Customer c = new Customer();
        c.setFirstName("Santanu");
        c.setLastName("Adhikari");
        c.setId(id);
        c.setPhoneNumber("9620170364");
		
        exchange.getOut().setBody( new Object[] {c} );
	}

}
