package com.bizruntime;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import com.bizruntime.POJO1.Customer;

public class UpdateCustomerProcessor implements Processor{

	Logger log = Logger.getLogger(UpdateCustomerProcessor.class);
	@Override
	public void process(Exchange exchange) throws Exception {
		
		Customer customer = (Customer) exchange.getIn().getBody(Object[].class)[0];	
		log.info("Updating customer "+customer.getFirstName()+" "+customer.getLastName());
		
		exchange.getOut().setBody(new Object[] {});
		
	}

}
