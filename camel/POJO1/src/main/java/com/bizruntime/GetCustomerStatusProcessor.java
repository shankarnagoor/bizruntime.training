package com.bizruntime;

import javax.xml.ws.Holder;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

public class GetCustomerStatusProcessor implements Processor{

	Logger log = Logger.getLogger(GetCustomerStatusProcessor.class);
	@Override
	public void process(Exchange exchange) throws Exception {
		Object[] obj = exchange.getIn().getBody(Object[].class);
		String id = (String) obj[0];
		Holder<String> status = (Holder<String>) obj[1];
		Holder<String> statusMsg = (Holder<String>) obj[2];
		
		log.debug("Getting status for customer '" + id + "'");
		
		status.value = "Offline";
		statusMsg.value = "going to sleep now...";
		
		exchange.getOut().setBody( new Object[]{ status,statusMsg} );
	}

}
