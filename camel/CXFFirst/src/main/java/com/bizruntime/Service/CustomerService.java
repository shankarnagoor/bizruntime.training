package com.bizruntime.Service;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Holder;

import com.bizruntime.Customer;

@WebService(targetNamespace = "http://bizruntime.com/wsdl/CustomerService/", name = "CustomerService", serviceName = "CustomerService", portName = "SOAPOverHTTP")
public interface CustomerService {

	public Customer lookupCustomer(
			@WebParam(name = "customerId", targetNamespace = "") String customerId);

	public void updateCustomer(
			@WebParam(name = "cust", targetNamespace = "") Customer cust);

	public void getCustomerStatus(
			@WebParam(name = "customerId", targetNamespace = "") String customerId,
			@WebParam(mode = WebParam.Mode.OUT, name = "status", targetNamespace = "") Holder<String> status,
			@WebParam(mode = WebParam.Mode.OUT, name = "statusMessage", targetNamespace = "") Holder<String> statusMessage);
}
