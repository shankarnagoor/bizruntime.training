package com.bizruntime.Service;

import javax.xml.ws.Holder;
import org.apache.log4j.Logger;

import com.bizruntime.Customer;
import com.bizruntime.Service.CustomerService;

public class CustomerServiceImpl implements CustomerService {

	Logger log = Logger.getLogger(CustomerServiceImpl.class);

	@Override
	public Customer lookupCustomer(String customerId) {
		log.debug("Returning information for customer " + customerId);

		Customer customer = new Customer("Shankar", "Nagoor", "9845439318",
				customerId);
		return customer;
	}

	@Override
	public void updateCustomer(Customer cust) {
		log.info("updating customer :" + cust.getFirstName());

	}

	@Override
	public void getCustomerStatus(String customerId, Holder<String> status,
			Holder<String> statusMessage) {

		log.info("Getting status for customer " + customerId);
		status.value = "Active";
		statusMessage.value = "My 1st web Service using camel..";
	}

}
