package com.bizruntime.CXFFirst;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
