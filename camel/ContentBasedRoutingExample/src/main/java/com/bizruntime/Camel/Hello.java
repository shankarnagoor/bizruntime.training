package com.bizruntime.Camel;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
