package com.bizruntime.Camel;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ContentBasedRoutingExample {
	public void test() throws Exception {
		ApplicationContext appContext = new ClassPathXmlApplicationContext(
				"META-INF/applicationContext.xml");
		CamelContext camelContext = SpringCamelContext.springCamelContext(
				appContext, false);
		try {
			ProducerTemplate template = camelContext.createProducerTemplate();
			camelContext.start();
			template.sendBody("direct:start", "Java Threads");
			template.sendBodyAndHeader("direct:start",
					"Camel Content Based Router", "views", 20000);
			template.sendBody("direct:start", "Spring Integration");
		} finally {
			camelContext.stop();
		}
	}

	public static void main(String[] args) {
		ContentBasedRoutingExample ctr = new ContentBasedRoutingExample();
		try {
			ctr.test();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
