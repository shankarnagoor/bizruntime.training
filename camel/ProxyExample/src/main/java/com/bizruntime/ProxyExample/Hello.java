package com.bizruntime.ProxyExample;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
