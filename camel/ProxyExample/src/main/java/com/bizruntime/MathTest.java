package com.bizruntime;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

public class MathTest implements Processor {

	Logger log=Logger.getLogger(MathTest.class);
	public void process(Exchange exchange) throws Exception {
		Object[] msg=exchange.getIn().getBody(Object[].class);
		 log.info(msg);
		 int a=(Integer)msg[0];
		 int b=(Integer)msg[1];
		 
		 log.info("Addition:"+(a+b));
		 
		 exchange.getOut().setBody(a+b);
		
		
		
	}
}
