package com.bizruntime.POJO;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
