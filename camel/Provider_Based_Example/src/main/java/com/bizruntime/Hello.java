package com.bizruntime;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
