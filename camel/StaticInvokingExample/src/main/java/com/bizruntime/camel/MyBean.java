package com.bizruntime.camel;

import org.apache.camel.Header;

public final class MyBean {

	public String hello(String name) {
		return "Hello " + name;
	}

	public String hello(String name, @Header("country") String country) {
		return "Hello " + name + " you are from " + country;
	}

	public String times(String name, @Header("times") int times) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < times; i++) {
			sb.append(name);
		}
		return sb.toString();
	}

	public String times(byte[] data, @Header("times") int times) {
		String s = new String(data);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < times; i++) {
			sb.append(s);
			if (i < times - 1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}

	public String times(String name, int times, char separator) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < times; i++) {
			sb.append(name);
			if (i < times - 1) {
				sb.append(separator);
			}
		}
		return sb.toString();
	}

}