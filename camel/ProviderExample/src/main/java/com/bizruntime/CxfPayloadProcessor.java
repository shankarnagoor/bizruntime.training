package com.bizruntime;

import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.cxf.binding.soap.SoapHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CxfPayloadProcessor implements Processor{

	@Override
	public void process(Exchange ex) throws Exception {
		 Document xml = ex.getIn().getBody(Document.class);
	     List<Element> elements = new ArrayList<Element>();
	     elements.add(xml.getDocumentElement());
	     ex.getOut().setBody(new CxfPayload<SoapHeader>(null, elements));
	}

}
