package com.bizruntime;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class GetCustomerStatusProcessor implements Processor{

	@Override
	public void process(Exchange ex) throws Exception {
		String id = ex.getIn().getHeader("customerId", String.class);

		ex.getIn().setHeader("status", "good");
		ex.getIn().setHeader("statusMessage", "feeling happy");
		
	}

}
