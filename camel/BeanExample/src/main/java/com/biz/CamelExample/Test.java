package com.biz.CamelExample;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

public class Test implements Processor{

	private static Logger logger=Logger.getLogger(Test.class);	
	
	public void show(){
		logger.info("Got it..");
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		Endpoint ep=exchange.getFromEndpoint();
		String str = exchange.getIn().getBody(String.class);
		System.out.println("End point :"+ep);
		System.out.println("Body :"+str);
		System.out.println(exchange.getIn().getHeader("myHeader"));
		
	}
}
