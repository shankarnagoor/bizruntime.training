package com.bizruntime.SplitExample;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
