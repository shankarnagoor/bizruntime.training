package com.bizruntime.example;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.spring.SpringCamelContext;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
 
public class CamelRecipientListExampleUsingSpring {
	static Logger log= Logger.getLogger(CamelRecipientListExampleUsingSpring.class);

	public void test() throws Exception{
		
        ApplicationContext appContext = new ClassPathXmlApplicationContext(
                "src/main/resources/META-INF/applicationContext.xml");
        CamelContext camelContext = SpringCamelContext.springCamelContext(
                appContext, false);
        try {
            ProducerTemplate template = camelContext.createProducerTemplate();           
            camelContext.start();
            log.info("************************");
            Employee sam = new Employee("Sam");
            sam.setNew(true);
            sam.setMessage("Joined");
             
            template.sendBody("direct:start", sam);
             
            log.info("************************");
             
            Employee john = new Employee("John");
            john.setOnLeave(true);
            john.setMessage("On Leave");
            template.sendBody("direct:start", john);
             
            log.info("************************");
             
            Employee roy = new Employee("Roy");
            roy.setPromoted(true);
            roy.setMessage("Promoted");
            template.sendBody("direct:start", roy);
             
            log.info("************************");
             
            Employee ram = new Employee("Ram");
            ram.setResigning(true);
            ram.setMessage("Resigning");
            template.sendBody("direct:start", ram);
             
            log.info("************************");
        } finally {
            camelContext.stop();
        }
    }
public static final void main(String[] args) throws Exception {
	CamelRecipientListExampleUsingSpring recp=new CamelRecipientListExampleUsingSpring();
	recp.test();
}
	
}