package com.bizruntime;

import java.util.Arrays;
import java.util.List;

import org.apache.camel.Header;
import org.apache.camel.spi.HeaderFilterStrategy.Direction;

public class CustomHeaderFilter implements MessageHeaderFilter {

	public static final String ACTIVATION_NAMESPACE =
			"http://cxf.apache.org/bindings/custom";
			public static final List<String> ACTIVATION_NAMESPACES =
			Arrays.asList(ACTIVATION_NAMESPACE);
	@Override
	public List<String> getActivationNamespaces() {
		return ACTIVATION_NAMESPACES;
	}

	@Override
	public void filter(Direction direction, List<Header> headers) {

	}

}
