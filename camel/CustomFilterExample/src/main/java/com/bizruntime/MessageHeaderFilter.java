package com.bizruntime;

import java.util.List;

import org.apache.camel.Header;
import org.apache.camel.spi.HeaderFilterStrategy.Direction;

public interface MessageHeaderFilter 
{
	List<String> getActivationNamespaces();
	void filter(Direction direction, List<Header> headers);
}
