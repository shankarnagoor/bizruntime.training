package com.bizruntime.PollEnricher;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
