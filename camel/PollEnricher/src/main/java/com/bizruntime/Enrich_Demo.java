package com.bizruntime;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class Enrich_Demo implements AggregationStrategy {
	@Override
	public Exchange aggregate(Exchange original, Exchange resource) {
		String originalBody = original.getIn().getBody(String.class);
		String resourceBody = original.getIn().getBody(String.class);
		String merge = originalBody + "" + resourceBody;
		if (original.getPattern().isOutCapable()) {
			original.getOut().setBody(merge);
		} else {
			original.getIn().setBody(merge);
		}
		return original;

	}
}
