package com.bizruntime.WSDLtoJava;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
