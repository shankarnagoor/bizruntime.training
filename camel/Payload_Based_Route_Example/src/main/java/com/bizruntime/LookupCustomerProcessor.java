package com.bizruntime;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;


public class LookupCustomerProcessor implements Processor{

	@Override
	public void process(Exchange ex) throws Exception {
		String body = ex.getIn().getBody(String.class);
		System.out.println("Message Body :"+body);
		
		String id = ex.getIn().getHeader("customerId", String.class);
		System.out.println("customerId :"+id);
		
		 ex.getIn().setHeader("firstName", "Shankar");
		 ex.getIn().setHeader("lastName", "Nagoor");
		 ex.getIn().setHeader("phoneNumber", "9845439318");
		 ex.getIn().setHeader("id", id);
	}

}
