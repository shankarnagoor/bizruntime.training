package com.bizruntime;

import javax.xml.ws.Holder;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

public class GetCustomerStatusProcessor implements Processor{

	Logger log = Logger.getLogger(GetCustomerStatusProcessor.class);
	@Override
	public void process(Exchange exchange) throws Exception {
		String id = exchange.getIn().getHeader("customerId",String.class);
				
		exchange.getIn().setHeader("status", "Away");
		exchange.getIn().setHeader("statusMessage", "Going to sleep.");			
	}

}
