package com.bizruntime.camel;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
