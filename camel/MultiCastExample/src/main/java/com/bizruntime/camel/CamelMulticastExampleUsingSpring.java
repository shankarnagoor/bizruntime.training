package com.bizruntime.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CamelMulticastExampleUsingSpring {
	public void test() throws Exception {
		ApplicationContext appContext = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		CamelContext camelContext = SpringCamelContext.springCamelContext(
				appContext, false);
		try {
			ProducerTemplate template = camelContext.createProducerTemplate();
			camelContext.start();
			template.sendBody("direct:start", "Multicast");
		} finally {
			camelContext.stop();
		}
	}

	public static final void main(String[] args) throws Exception {
		CamelMulticastExampleUsingSpring testsp = new CamelMulticastExampleUsingSpring();
		testsp.test();
	}

}
