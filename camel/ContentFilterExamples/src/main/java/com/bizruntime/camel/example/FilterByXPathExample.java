package com.bizruntime.camel.example;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.builder.xml.XPathBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultProducerTemplate;

public class FilterByXPathExample {
	public void test() {
		final String articlesXml = "<blog><article><category>spring integration</category><title>SpringInt Splitter</title></article>"
				+ "<article><category>java</category><title>Lambda</title></article>"
				+ "<article><category>camel</category><title>Camel Multicast</title></article>"
				+ "<article><category>camel</category><title>Camel Component: ActiveMQ </title></article>"
				+ "<article><category>camel</category><title>Camel Component: Timer</title></article>"
				+ "<article><category>camel</category><title>Camel Component: Logger</title></article>"
				+ "<article><category>camel</category><title>Camel DSL</title></article></blog>";
		CamelContext camelContext = new DefaultCamelContext();
		try {
			camelContext.addRoutes(new RouteBuilder() {
				public void configure() {
					XPathBuilder splitter = new XPathBuilder("//blog/article");
					from("direct:xpath").split(splitter).filter()
							.xquery("//article[category='camel']")
							.to("direct:camel");

					from("direct:camel")
							.filter()
							.xpath("//article/title[contains(.,'Camel Component')]")
							.to("stream:out");

				}
			});
			camelContext.start();
			ProducerTemplate template = new DefaultProducerTemplate(
					camelContext);
			template.start();
			template.sendBody("direct:xpath", articlesXml);
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				camelContext.stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		FilterByXPathExample path = new FilterByXPathExample();
		path.test();
	}
}
