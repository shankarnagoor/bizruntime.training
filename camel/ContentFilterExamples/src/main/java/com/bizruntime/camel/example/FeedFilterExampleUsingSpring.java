package com.bizruntime.camel.example;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FeedFilterExampleUsingSpring {
	CamelContext camelContext;

	public void test() {
		ApplicationContext appContext = new ClassPathXmlApplicationContext(
				"applicationContext.xml");

		try {
			camelContext = SpringCamelContext.springCamelContext(appContext,
					false);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			camelContext.start();
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				camelContext.stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		FeedFilterExampleUsingSpring feedfilter = new FeedFilterExampleUsingSpring();
		feedfilter.test();

	}

}
