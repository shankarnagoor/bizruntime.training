package com.bizruntime.camel.example;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultProducerTemplate;
import org.apache.log4j.Logger;

public class FilterByPredicateExample {
	static Logger log = Logger.getLogger(FilterByPredicateExample.class);

	public void test() {
		CamelContext camelContext = new DefaultCamelContext();
		try {
			camelContext.addRoutes(new RouteBuilder() {
				public void configure() {
					from("direct:start").filter(new Predicate() {

						public boolean matches(Exchange exchange) {
							final String body = exchange.getIn().getBody(
									String.class);
							return ((body != null) && body.startsWith("Camel"));
						}
					}).to("stream:out");
				}
			});
			camelContext.start();
			ProducerTemplate template = new DefaultProducerTemplate(
					camelContext);
			template.start();
			template.sendBody("direct:start", "Camel Multicast");
			template.sendBody("direct:start", "Camel Components");
			template.sendBody("direct:start", "Spring Integration");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				camelContext.stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		FilterByPredicateExample filter = new FilterByPredicateExample();
		filter.test();
	}
}
