package com.bizruntime.camel.example;

import org.apache.abdera.model.Entry;
import org.apache.camel.Exchange;
import org.apache.log4j.Logger;


public class CamelArticles {
	static Logger log=Logger.getLogger(CamelArticles.class);
      
	   public boolean filter(Exchange exchange) {
        Entry entry = exchange.getIn().getBody(Entry.class);
        String title = entry.getTitle();
        boolean camelArticles = title.toLowerCase().startsWith("camel");
        if (camelArticles) {
            log.info("allow " + title);
        }
        return camelArticles;
    }
    
    public boolean filterOnlyCamelComponents(String body) {       
        boolean camelArticles = body.toLowerCase().startsWith("camel component");
        return camelArticles;
    }
}
