package com.bizruntime.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CamelLoadBalanceRoundRobinUsingSpring {
    public void test() throws Exception{
        ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext(
                "loadBalancingRoundRobinCamelContext.xml");
        CamelContext camelContext = SpringCamelContext.springCamelContext(
                appContext, false);
        try {            
            camelContext.start();            
            Thread.sleep(6000);
        } finally {
            camelContext.stop();
            appContext.close();
        }
    }

public static final void main(String[] args) throws Exception {
	CamelLoadBalanceRoundRobinUsingSpring rrb=new CamelLoadBalanceRoundRobinUsingSpring();
	rrb.test();
}
}
