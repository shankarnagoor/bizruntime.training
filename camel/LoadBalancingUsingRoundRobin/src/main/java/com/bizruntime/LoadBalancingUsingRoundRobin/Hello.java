package com.bizruntime.LoadBalancingUsingRoundRobin;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
