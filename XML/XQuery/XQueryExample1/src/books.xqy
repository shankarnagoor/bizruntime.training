let $books := (doc("src/books.xml")/books/book)
return <results>
{
   for $x in $books
   where $x/price>500
   order by $x/price
   return $x/title
}
</results>