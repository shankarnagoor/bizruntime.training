
/**
 * AreaServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.1  Built on : Feb 20, 2016 (10:01:29 GMT)
 */
    package org.tempuri.areaservice;
    /**
     *  AreaServiceSkeleton java skeleton for the axisService
     */
    public class AreaServiceSkeleton{
        
         
        /**
         * Auto generated method signature
         * 
                                     * @param parameters 
             * @return area 
         */
        
                 public org.tempuri.areaservice.Area calculateRectArea
                  (
                  org.tempuri.areaservice.Parameters parameters
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#calculateRectArea");
        }
     
    }
    