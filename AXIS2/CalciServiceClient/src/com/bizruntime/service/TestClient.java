package com.bizruntime.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import org.apache.axis.client.Service;





public class TestClient {
	public static void main(String[] args) throws MalformedURLException, RemoteException {
		URL url=new URL("http://localhost:8080/CalciService/services/CalciService");
		Service service= new Service();
		  
		CalciServiceSoapBindingStub stub = new CalciServiceSoapBindingStub(url,service);
		int n1=20,n2=5;
		int res1 = stub.addition(n1,n2);
		System.out.println("Sum of two numbers is: "+res1);
		
		int res2 = stub.substract(n1, n2);
		System.out.println("Difference between two no is: "+res2);
		
		int res3 = stub.multiply(n1, n2);
		System.out.println("Multplication of two numbers is: "+res3);
		
		int res4 = stub.addition(n1, n2);
		System.out.println("Division is: "+res4);
		
		
	}

}
