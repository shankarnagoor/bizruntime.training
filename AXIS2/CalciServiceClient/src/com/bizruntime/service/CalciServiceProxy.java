package com.bizruntime.service;

public class CalciServiceProxy implements com.bizruntime.service.CalciService {
  private String _endpoint = null;
  private com.bizruntime.service.CalciService calciService = null;
  
  public CalciServiceProxy() {
    _initCalciServiceProxy();
  }
  
  public CalciServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initCalciServiceProxy();
  }
  
  private void _initCalciServiceProxy() {
    try {
      calciService = (new com.bizruntime.service.CalciServiceServiceLocator()).getCalciService();
      if (calciService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)calciService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)calciService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (calciService != null)
      ((javax.xml.rpc.Stub)calciService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.bizruntime.service.CalciService getCalciService() {
    if (calciService == null)
      _initCalciServiceProxy();
    return calciService;
  }
  
  public int multiply(int n1, int n2) throws java.rmi.RemoteException{
    if (calciService == null)
      _initCalciServiceProxy();
    return calciService.multiply(n1, n2);
  }
  
  public int division(int n1, int n2) throws java.rmi.RemoteException{
    if (calciService == null)
      _initCalciServiceProxy();
    return calciService.division(n1, n2);
  }
  
  public int substract(int n1, int n2) throws java.rmi.RemoteException{
    if (calciService == null)
      _initCalciServiceProxy();
    return calciService.substract(n1, n2);
  }
  
  public int addition(int n1, int n2) throws java.rmi.RemoteException{
    if (calciService == null)
      _initCalciServiceProxy();
    return calciService.addition(n1, n2);
  }
  
  
}