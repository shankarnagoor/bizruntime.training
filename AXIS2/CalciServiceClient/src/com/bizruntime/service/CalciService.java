/**
 * CalciService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bizruntime.service;

public interface CalciService extends java.rmi.Remote {
    public int multiply(int n1, int n2) throws java.rmi.RemoteException;
    public int division(int n1, int n2) throws java.rmi.RemoteException;
    public int substract(int n1, int n2) throws java.rmi.RemoteException;
    public int addition(int n1, int n2) throws java.rmi.RemoteException;
}
