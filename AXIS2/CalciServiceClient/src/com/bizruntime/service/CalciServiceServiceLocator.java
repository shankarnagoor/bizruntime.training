/**
 * CalciServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bizruntime.service;

public class CalciServiceServiceLocator extends org.apache.axis.client.Service implements com.bizruntime.service.CalciServiceService {

    public CalciServiceServiceLocator() {
    }


    public CalciServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CalciServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CalciService
    private java.lang.String CalciService_address = "http://localhost:8080/CalciService/services/CalciService";

    public java.lang.String getCalciServiceAddress() {
        return CalciService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CalciServiceWSDDServiceName = "CalciService";

    public java.lang.String getCalciServiceWSDDServiceName() {
        return CalciServiceWSDDServiceName;
    }

    public void setCalciServiceWSDDServiceName(java.lang.String name) {
        CalciServiceWSDDServiceName = name;
    }

    public com.bizruntime.service.CalciService getCalciService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CalciService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCalciService(endpoint);
    }

    public com.bizruntime.service.CalciService getCalciService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.bizruntime.service.CalciServiceSoapBindingStub _stub = new com.bizruntime.service.CalciServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getCalciServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCalciServiceEndpointAddress(java.lang.String address) {
        CalciService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.bizruntime.service.CalciService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.bizruntime.service.CalciServiceSoapBindingStub _stub = new com.bizruntime.service.CalciServiceSoapBindingStub(new java.net.URL(CalciService_address), this);
                _stub.setPortName(getCalciServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CalciService".equals(inputPortName)) {
            return getCalciService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.bizruntime.com", "CalciServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.bizruntime.com", "CalciService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CalciService".equals(portName)) {
            setCalciServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
