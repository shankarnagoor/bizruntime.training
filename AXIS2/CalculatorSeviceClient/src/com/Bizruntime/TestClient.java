package com.Bizruntime;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import org.apache.axis.AxisFault;
import org.apache.axis.client.Service;

public class TestClient {
public static void main(String[] args) throws MalformedURLException, RemoteException {

	
	URL url= new URL("http://localhost:8080/SimpleCalulator/services/CalculatorService");
			
			Service service= new Service();
			  
			CalculatorServiceSoapBindingStub stub = new CalculatorServiceSoapBindingStub(url,service);
	int result = stub.add(10, 20);
	System.out.println("result : "+result);
}
}
;
