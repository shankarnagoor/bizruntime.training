
/**
 * ConverterServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.1  Built on : Feb 20, 2016 (10:01:29 GMT)
 */
    package com.bizruntime;
    /**
     *  ConverterServiceSkeleton java skeleton for the axisService
     */
    public class ConverterServiceSkeleton{
        
         
        /**
         * Auto generated method signature
         * 
                                     * @param celsiusToFarenheit 
             * @return celsiusToFarenheitResponse 
         */
        
                 public com.bizruntime.CelsiusToFarenheitResponse celsiusToFarenheit
                  (
                  com.bizruntime.CelsiusToFarenheit celsiusToFarenheit
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#celsiusToFarenheit");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param farenheitToCelsius 
             * @return farenheitToCelsiusResponse 
         */
        
                 public com.bizruntime.FarenheitToCelsiusResponse farenheitToCelsius
                  (
                  com.bizruntime.FarenheitToCelsius farenheitToCelsius
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#farenheitToCelsius");
        }
     
    }
    