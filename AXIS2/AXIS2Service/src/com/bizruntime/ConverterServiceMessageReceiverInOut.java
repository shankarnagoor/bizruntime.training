
/**
 * ConverterServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.1  Built on : Feb 20, 2016 (10:01:29 GMT)
 */
        package com.bizruntime;

        /**
        *  ConverterServiceMessageReceiverInOut message receiver
        */

        public class ConverterServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        ConverterServiceSkeleton skel = (ConverterServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("celsiusToFarenheit".equals(methodName)){
                
                com.bizruntime.CelsiusToFarenheitResponse celsiusToFarenheitResponse1 = null;
	                        com.bizruntime.CelsiusToFarenheit wrappedParam =
                                                             (com.bizruntime.CelsiusToFarenheit)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.bizruntime.CelsiusToFarenheit.class);
                                                
                                               celsiusToFarenheitResponse1 =
                                                   
                                                   
                                                         skel.celsiusToFarenheit(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), celsiusToFarenheitResponse1, false,
                                                    new javax.xml.namespace.QName("http://bizruntime.com", "CelsiusToFarenheitResponse"));
                                    } else 

            if("farenheitToCelsius".equals(methodName)){
                
                com.bizruntime.FarenheitToCelsiusResponse farenheitToCelsiusResponse3 = null;
	                        com.bizruntime.FarenheitToCelsius wrappedParam =
                                                             (com.bizruntime.FarenheitToCelsius)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.bizruntime.FarenheitToCelsius.class);
                                                
                                               farenheitToCelsiusResponse3 =
                                                   
                                                   
                                                         skel.farenheitToCelsius(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), farenheitToCelsiusResponse3, false,
                                                    new javax.xml.namespace.QName("http://bizruntime.com", "FarenheitToCelsiusResponse"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.CelsiusToFarenheit param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.CelsiusToFarenheit.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.CelsiusToFarenheitResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.CelsiusToFarenheitResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.FarenheitToCelsius param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.FarenheitToCelsius.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.FarenheitToCelsiusResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.FarenheitToCelsiusResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.bizruntime.CelsiusToFarenheitResponse param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.bizruntime.CelsiusToFarenheitResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.bizruntime.CelsiusToFarenheitResponse wrapCelsiusToFarenheit(){
                                com.bizruntime.CelsiusToFarenheitResponse wrappedElement = new com.bizruntime.CelsiusToFarenheitResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.bizruntime.FarenheitToCelsiusResponse param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.bizruntime.FarenheitToCelsiusResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.bizruntime.FarenheitToCelsiusResponse wrapFarenheitToCelsius(){
                                com.bizruntime.FarenheitToCelsiusResponse wrappedElement = new com.bizruntime.FarenheitToCelsiusResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type) throws org.apache.axis2.AxisFault{

        try {
        
                if (com.bizruntime.CelsiusToFarenheit.class.equals(type)){
                
                        return com.bizruntime.CelsiusToFarenheit.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.CelsiusToFarenheitResponse.class.equals(type)){
                
                        return com.bizruntime.CelsiusToFarenheitResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.FarenheitToCelsius.class.equals(type)){
                
                        return com.bizruntime.FarenheitToCelsius.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.FarenheitToCelsiusResponse.class.equals(type)){
                
                        return com.bizruntime.FarenheitToCelsiusResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    