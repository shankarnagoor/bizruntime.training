package com.bizruntime;

import java.rmi.RemoteException;

import com.bizruntime.ConverterServiceStub.FarenheitToCelsius;
public class TestClient {

	public static void main(String[] args) throws RemoteException {
		ConverterServiceStub stub=new ConverterServiceStub();
		
		ConverterServiceStub.CelsiusToFarenheit f1=new ConverterServiceStub.CelsiusToFarenheit();
		ConverterServiceStub.FarenheitToCelsius c1=new FarenheitToCelsius();
		
		f1.setCelsius(20);
		c1.setFarenheit(35);
		ConverterServiceStub.CelsiusToFarenheitResponse res1=stub.celsiusToFarenheit(f1);
		ConverterServiceStub.FarenheitToCelsiusResponse res2=stub.farenheitToCelsius(c1);
		float f=res1.getCelsiusToFarenheitReturn();
		float c=res2.getFarenheitToCelsiusReturn();
		
		System.out.println("convertion from celcius to Faranheit:"+f);
		System.out.println("convertion from faranheit to celcius:"+c);
		
	}

}
