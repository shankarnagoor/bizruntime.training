
/**
 * CalciServiceServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.1  Built on : Feb 20, 2016 (10:01:29 GMT)
 */
        package com.bizruntime.service;

        /**
        *  CalciServiceServiceMessageReceiverInOut message receiver
        */

        public class CalciServiceServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        CalciServiceServiceSkeleton skel = (CalciServiceServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("addition".equals(methodName)){
                
                com.bizruntime.service.AdditionResponse additionResponse1 = null;
	                        com.bizruntime.service.Addition wrappedParam =
                                                             (com.bizruntime.service.Addition)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.bizruntime.service.Addition.class);
                                                
                                               additionResponse1 =
                                                   
                                                   
                                                         skel.addition(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), additionResponse1, false,
                                                    new javax.xml.namespace.QName("http://service.bizruntime.com", "additionResponse"));
                                    } else 

            if("division".equals(methodName)){
                
                com.bizruntime.service.DivisionResponse divisionResponse3 = null;
	                        com.bizruntime.service.Division wrappedParam =
                                                             (com.bizruntime.service.Division)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.bizruntime.service.Division.class);
                                                
                                               divisionResponse3 =
                                                   
                                                   
                                                         skel.division(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), divisionResponse3, false,
                                                    new javax.xml.namespace.QName("http://service.bizruntime.com", "divisionResponse"));
                                    } else 

            if("substract".equals(methodName)){
                
                com.bizruntime.service.SubstractResponse substractResponse5 = null;
	                        com.bizruntime.service.Substract wrappedParam =
                                                             (com.bizruntime.service.Substract)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.bizruntime.service.Substract.class);
                                                
                                               substractResponse5 =
                                                   
                                                   
                                                         skel.substract(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), substractResponse5, false,
                                                    new javax.xml.namespace.QName("http://service.bizruntime.com", "substractResponse"));
                                    } else 

            if("multiply".equals(methodName)){
                
                com.bizruntime.service.MultiplyResponse multiplyResponse7 = null;
	                        com.bizruntime.service.Multiply wrappedParam =
                                                             (com.bizruntime.service.Multiply)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.bizruntime.service.Multiply.class);
                                                
                                               multiplyResponse7 =
                                                   
                                                   
                                                         skel.multiply(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), multiplyResponse7, false,
                                                    new javax.xml.namespace.QName("http://service.bizruntime.com", "multiplyResponse"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.service.Addition param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.service.Addition.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.service.AdditionResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.service.AdditionResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.service.Division param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.service.Division.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.service.DivisionResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.service.DivisionResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.service.Substract param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.service.Substract.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.service.SubstractResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.service.SubstractResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.service.Multiply param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.service.Multiply.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.bizruntime.service.MultiplyResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.bizruntime.service.MultiplyResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.bizruntime.service.AdditionResponse param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.bizruntime.service.AdditionResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.bizruntime.service.AdditionResponse wrapaddition(){
                                com.bizruntime.service.AdditionResponse wrappedElement = new com.bizruntime.service.AdditionResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.bizruntime.service.DivisionResponse param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.bizruntime.service.DivisionResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.bizruntime.service.DivisionResponse wrapdivision(){
                                com.bizruntime.service.DivisionResponse wrappedElement = new com.bizruntime.service.DivisionResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.bizruntime.service.SubstractResponse param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.bizruntime.service.SubstractResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.bizruntime.service.SubstractResponse wrapsubstract(){
                                com.bizruntime.service.SubstractResponse wrappedElement = new com.bizruntime.service.SubstractResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.bizruntime.service.MultiplyResponse param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.bizruntime.service.MultiplyResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.bizruntime.service.MultiplyResponse wrapmultiply(){
                                com.bizruntime.service.MultiplyResponse wrappedElement = new com.bizruntime.service.MultiplyResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type) throws org.apache.axis2.AxisFault{

        try {
        
                if (com.bizruntime.service.Addition.class.equals(type)){
                
                        return com.bizruntime.service.Addition.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.service.AdditionResponse.class.equals(type)){
                
                        return com.bizruntime.service.AdditionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.service.Division.class.equals(type)){
                
                        return com.bizruntime.service.Division.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.service.DivisionResponse.class.equals(type)){
                
                        return com.bizruntime.service.DivisionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.service.Multiply.class.equals(type)){
                
                        return com.bizruntime.service.Multiply.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.service.MultiplyResponse.class.equals(type)){
                
                        return com.bizruntime.service.MultiplyResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.service.Substract.class.equals(type)){
                
                        return com.bizruntime.service.Substract.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.bizruntime.service.SubstractResponse.class.equals(type)){
                
                        return com.bizruntime.service.SubstractResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    