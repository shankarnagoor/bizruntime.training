
/**
 * CalciServiceServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.1  Built on : Feb 20, 2016 (10:01:29 GMT)
 */
    package com.bizruntime.service;
    /**
     *  CalciServiceServiceSkeleton java skeleton for the axisService
     */
    public class CalciServiceServiceSkeleton{
        
         
        /**
         * Auto generated method signature
         * 
                                     * @param addition 
             * @return additionResponse 
         */
        
                 public com.bizruntime.service.AdditionResponse addition
                  (
                  com.bizruntime.service.Addition addition
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#addition");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param division 
             * @return divisionResponse 
         */
        
                 public com.bizruntime.service.DivisionResponse division
                  (
                  com.bizruntime.service.Division division
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#division");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param substract 
             * @return substractResponse 
         */
        
                 public com.bizruntime.service.SubstractResponse substract
                  (
                  com.bizruntime.service.Substract substract
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#substract");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param multiply 
             * @return multiplyResponse 
         */
        
                 public com.bizruntime.service.MultiplyResponse multiply
                  (
                  com.bizruntime.service.Multiply multiply
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#multiply");
        }
     
    }
    