package com.bizruntime;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService(endpointInterface="com.bizruntime.CalWebService")
@SOAPBinding(style=Style.RPC)
public class CalWEbServiceImpl implements CalWebService {

	@Override
	public int add(int x, int y) {
		
		return x+y;
	}

}
