package com.bizruntime;

//import java.net.URI;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class CalClient {
	public static void main(String[] args) throws MalformedURLException {
		URL url=new URL("http://localhost:8080/Cal?wsdl");
		QName qname=new QName("http://bizruntime.com/","CalWEbServiceImplService");
	    Service service= Service.create(url,qname);
	    CalWebService calService=service.getPort(CalWebService.class);
	    System.out.println(calService.add(20, 30));
	
	}

}
