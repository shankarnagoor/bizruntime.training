package com.webservice;

import javax.jws.WebService;

@WebService(endpointInterface="com.webservice.ISubstraction")

public class SubstractionImpl implements ISubstraction {

	public int substract(int x, int y) {
		int z=x-y;  
		return z;
	}

}
