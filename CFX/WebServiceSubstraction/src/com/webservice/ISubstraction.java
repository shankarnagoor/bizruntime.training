package com.webservice;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(name = "ISubstraction", targetNamespace = "http://webservice.com/")
public interface ISubstraction  {
	@WebMethod(operationName = "substract", action = "urn:Substract")
	public int substract(int x,int y);

}
