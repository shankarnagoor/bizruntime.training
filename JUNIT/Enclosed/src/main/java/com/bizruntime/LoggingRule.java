package com.bizruntime;

import org.apache.log4j.Logger;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
 
public class LoggingRule implements TestRule {
 
 public class LoggingStatement extends Statement {
	 final Logger log= Logger.getLogger(LoggingStatement.class);
 
  private final Statement statement;
 
  public LoggingStatement(Statement aStatement, String aName) {
   statement = aStatement;
  }
 
  @Override
  public void evaluate() throws Throwable {
   log.info("before: " + name);
   statement.evaluate();
   log.info("after: " + name);
  }
 
 }
 
 private final String name;
 
 public LoggingRule(String aName) {
  name = aName;
 }
 
 public Statement apply(Statement statement, Description description) {
	 final Logger log= Logger.getLogger(LoggingStatement.class);
  log.info("apply: " + name);
 
  return new LoggingStatement(statement, name);
 }
 
}

