package com.bizruntime;

import org.apache.log4j.Logger;

public class MessageUtil {
	final static Logger logger = Logger.getLogger(MathUtils.class);
	   private String message;

	   //Constructor
	   //@param message to be printed
	   public MessageUtil(String message){
	      this.message = message; 
	   }

	   // prints the message
	   public void printMessage(){
	      logger.info(message);
	      while(true);
	   }   

	   // add "Hi!" to the message
	   public String salutationMessage(){
	      message = "Hi!" + message;
	      logger.info(message);
	      return message;
	   }   
	}
