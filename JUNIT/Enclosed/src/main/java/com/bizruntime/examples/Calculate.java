package com.bizruntime.examples;

import org.apache.log4j.Logger;

public class Calculate {
	static Logger logger = Logger.getLogger(Calculate.class);

	public int add(int var1, int var2) {

		logger.info("adding values: " + var1 + "+" + var2);
		return var1 + var2;
	}

	public int multiply(int var1, int var2) {

		logger.info("multiply values: " + var1 + "*" + var2);
		return var1 * var2;
	}

	public int substract(int var1, int var2) {

		logger.info("substract values: " + var1 + "-" + var2);
		return var1 - var2;
	}

	public int divide(int var1, int var2) {

		logger.info("divide values: " + var1 + "/" + var2);
		return var1 / var2;
	}
}
