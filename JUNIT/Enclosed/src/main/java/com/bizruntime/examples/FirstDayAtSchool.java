package com.bizruntime.examples;

import java.util.Arrays;

import org.apache.log4j.Logger;

public class FirstDayAtSchool {
	static Logger logger = Logger.getLogger(FirstDayAtSchool.class);

	public String[] prepareMyBag() {

		String[] schoolbag = { "Books", "Notebooks", "Pens" };

		logger.info("My school bag contains: " + Arrays.toString(schoolbag));

		return schoolbag;

	}

	public String[] addPencils() {

		String[] schoolbag = { "Books", "Notebooks", "Pens", "Pencils" };

		logger.info("Now my school bag contains: " + Arrays.toString(schoolbag));

		return schoolbag;

	}

}
