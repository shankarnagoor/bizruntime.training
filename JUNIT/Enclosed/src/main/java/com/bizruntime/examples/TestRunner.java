package com.bizruntime.examples;

import org.apache.log4j.Logger;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
	final static Logger log = Logger.getLogger(TestRunner.class);

	public void runner() {
		Result result = JUnitCore.runClasses(TestRunner.class);
		for (Failure failure : result.getFailures()) {
			log.info(failure.toString());
		}
		log.info(result.wasSuccessful());
	}

	public static void main(String[] args) {
		TestRunner tr = new TestRunner();
		tr.runner();

	}
}