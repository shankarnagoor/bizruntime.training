package com.bizruntime.examples;

import org.apache.log4j.Logger;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class ConsoleOutRule implements TestRule {
	static Logger logger = Logger.getLogger(ConsoleOutRule.class);

	private final String note;

	public ConsoleOutRule(final String note) {
		this.note = note;
	}

	public Statement apply(final Statement base, final Description description) {
		logger.info("rule applied. note: " + note);
		return base;
	}
}
