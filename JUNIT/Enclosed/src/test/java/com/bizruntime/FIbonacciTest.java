package com.bizruntime;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.bizruntime.examples.Fibonacci;

@RunWith(Parameterized.class)
public class FIbonacciTest {
	@Parameters(name = "{index}: fib[{2}]={1}")
	public static Iterable<Object[]> data() {
		return Arrays.asList(new Object[][] { { 1, 1 }, { 2, 1 }, { 3, 2 },
				{ 4, 3 }, { 5, 5 }, { 6, 8 } });
	}

	private int fInput;

	private int fExpected;

	public FIbonacciTest(int input, int expected) {
		fInput = input;
		fExpected = expected + input;
	}

	@Test
	public void test() {
		assertEquals(fExpected, Fibonacci.compute(fInput));
	}

	public int getCount() {

		return fExpected;

	}
}