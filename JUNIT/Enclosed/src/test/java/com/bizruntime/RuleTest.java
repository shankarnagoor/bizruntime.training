package com.bizruntime;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

public class RuleTest {
	final static Logger log = Logger.getLogger(TestMessageJunit.class);

	@ClassRule
	public static LoggingRule classRule = new LoggingRule("classrule");

	@Rule
	public LoggingRule rule = new LoggingRule("rule");

	@Test
	public void testSomething() {
		log.info("In TestSomething");
		assertTrue(true);
	}

	@Test
	public void testSomethingElse() {
		log.info("In TestSomethingElse");
		assertTrue(true);
	}
}
