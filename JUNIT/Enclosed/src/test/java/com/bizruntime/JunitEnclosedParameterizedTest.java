package com.bizruntime;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Enclosed.class)
public class JunitEnclosedParameterizedTest {
	final static Logger log = Logger.getLogger(JunitEnclosedParameterizedTest.class);

	static class Person {

		final String name;

		public Person(String name) {
			this.name = name;
			log.info(name);
		}

		@Override
		public String toString() {
			return String.format("Person[name: %s]", name);
		}
	}

	@RunWith(Parameterized.class)
	public static class PersonNameTest {

		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] { new Object[] { "Shankar" },
					new Object[] { "Harsha" }, });
		}

		String name;

		public PersonNameTest(String name) {
			this.name = name;
		}

		@Test
		public void createPersonWithName() {
			Person person = new Person(name);
			assertEquals(name, person.name);
		}
	}

	@RunWith(Parameterized.class)
	public static class PersonTest {

		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] {
					new Object[] { new Person("Christian") },
					new Object[] { new Person("Joshua") }, });
		}

		Person person;

		public PersonTest(Person person) {
			this.person = person;
		}

		@Test
		public void savePerson() {
			// TODO: Implementation test logic
		}
	}
}
