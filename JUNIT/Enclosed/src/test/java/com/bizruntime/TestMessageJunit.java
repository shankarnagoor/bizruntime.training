package com.bizruntime;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class TestMessageJunit {

	final static Logger log = Logger.getLogger(TestMessageJunit.class);

	String message = "Robert";
	MessageUtil messageUtil = new MessageUtil(message);

	@Test(timeout = 1000)
	public void testPrintMessage() {
		log.info("Inside testPrintMessage()");
		messageUtil.printMessage();
	}

	@Test
	public void testSalutationMessage() {
		log.info("Inside testSalutationMessage()");
		message = "Hi!" + "Robert";
		assertEquals(message, messageUtil.salutationMessage());
	}
}