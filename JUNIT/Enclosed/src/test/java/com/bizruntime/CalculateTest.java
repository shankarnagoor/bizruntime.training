package com.bizruntime;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.bizruntime.examples.Calculate;

public class CalculateTest {
	static Logger logger = Logger.getLogger(CalculateTest.class);

	Calculate calculation = new Calculate();

	int sum = calculation.add(2, 5);
	int testSum = 7;

	@Test
	public void testAdd() {
		logger.info("@Test sum(): " + sum + " = " + testSum);

		assertEquals(sum, testSum);
	}

	int mul = calculation.multiply(5, 5);
	int testMul = 25;

	@Test
	public void testMultiply() {
		logger.info("@Test mul(): " + mul + " = " + testMul);

		assertEquals(mul, testMul);
	}

	int sub = calculation.substract(199, 75);
	int testSub = 124;

	@Test
	public void testSubstract() {
		logger.info("@Test mul(): " + sub + " = " + testSub);

		assertEquals(sub, testSub);
	}

	int div = calculation.divide(200, 25);
	int testDiv = 8;

	@Test
	public void testDivide() {
		logger.info("@Test div(): " + div + " = " + testDiv);

		assertEquals(div, testDiv);
	}

}
