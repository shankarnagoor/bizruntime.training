package com.bizruntime;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.bizruntime.examples.FirstDayAtSchool;

public class FirstDayAtSchoolTest {
	static Logger logger = Logger.getLogger(FirstDayAtSchoolTest.class);

	FirstDayAtSchool school = new FirstDayAtSchool();

	String[] bag1 = { "Books", "Notebooks", "Pens" };

	String[] bag2 = { "Books", "Notebooks", "Pens", "Pencils" };

	@Test
	public void testPrepareMyBag() {

		logger.info("Inside testPrepareMyBag()");

		assertArrayEquals(bag1, school.prepareMyBag());

	}

	@Test
	public void testAddPencils() {

		logger.info("Inside testAddPencils()");

		assertArrayEquals(bag2, school.addPencils());

	}

}
