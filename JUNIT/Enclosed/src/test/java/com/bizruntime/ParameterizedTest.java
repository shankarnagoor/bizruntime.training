package com.bizruntime;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ParameterizedTest {
	final static Logger log = Logger.getLogger(Parameterized.class);

	static class Person {
		final String name;

		public Person(String name) {
			this.name = name;
			log.info(name);
		}

		@Override
		public String toString() {
			return String.format("Person[name: %s]", name);
		}
	}

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { new Object[] { "Shanakar" },
				new Object[] { "Harsh" }, });
	}

	String name;

	public ParameterizedTest(String name) {
		this.name = name;
	}

	@Test
	public void createPersonWithName() {
		Person person = new Person(name);
		assertEquals(name, person.name);
	}

}
