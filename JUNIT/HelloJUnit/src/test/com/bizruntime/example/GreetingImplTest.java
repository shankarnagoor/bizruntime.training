package com.bizruntime.example;

import static org.junit.Assert.*;

import org.junit.Test;

import com.bizruntime.example.Greeting;
import com.bizruntime.example.GreetingImpl;

public class GreetingImplTest {

	@Test
	public void greetShouldReturnValidOutput() {
		
	Greeting greeting=new GreetingImpl();
	String result=greeting.greet("JUnit");
	assertNotNull(result);
	assertEquals("Hello JUNIT",result);
	}
	
}
