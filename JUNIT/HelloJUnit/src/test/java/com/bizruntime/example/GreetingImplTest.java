package com.bizruntime.example;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.bizruntime.example.Greeting;
import com.bizruntime.example.GreetingImpl;

public class GreetingImplTest {
	static Logger log = Logger.getLogger(GreetingImplTest.class);

	private Greeting greeting;

	@Before
	public void setup() {
		greeting = new GreetingImpl();
		log.info("setup");

	}

	@Test
	public void greetShouldReturnValidOutput() {
		log.info("greetShouldReturnValidOutput");

		String result = greeting.greet("JUnit");
		assertNotNull(result);
		assertEquals("Hello JUnit", result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void greetShouldThrowAnException_For_NameIsNull() {
		log.info("greetShouldThrowAnException_For_NameIsNull");

		greeting.greet(null);

		greeting.greet("JUnit");

	}

	@Test(expected = IllegalArgumentException.class)
	public void greetShouldThrowAnException_For_NameIsBlank() {
		log.info("greetShouldThrowAnException_For_NameIsBlank");
		greeting.greet("");
	}

	@After
	public void tearDown() {
		log.info("tear down");
	}

}
