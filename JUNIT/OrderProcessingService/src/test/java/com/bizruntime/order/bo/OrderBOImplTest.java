package com.bizruntime.order.bo;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.bizruntime.order.bo.exception.BOException;
import com.bizruntime.order.dao.OrderDAO;
import com.bizruntime.order.dto.Order;

public class OrderBOImplTest {
	@Mock
	OrderDAO dao;
	private OrderBOImpl bo;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		bo = new OrderBOImpl();
		bo.setDao(dao);

	}

	@Test
	public void placeOrder_Should_Create_An_Order() throws SQLException,
			BOException {
		Order order = new Order();
		when(dao.create(order)).thenReturn(1);
		bo.placeOrder(order);
		assert (true);
		verify(dao).create(order);
	}

	@Test
	public void placeOrder_Should_NotCreateAnOrder() throws SQLException,
			BOException {
		Order order = new Order();
		when(dao.create(order)).thenReturn(new Integer(0));

		boolean result = bo.placeOrder(order);
		assertFalse(result);
		verify(dao).create(order);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void placeOrder_Should_Throw_BOException() throws SQLException,
			BOException {

		Order order = new Order();
		when(dao.create(order)).thenThrow(SQLException.class);
		bo.placeOrder(order);

	}

	@Test
	public void cancelOrder_Should_CancelOrder() throws SQLException,
			BOException {

		Order order = new Order();
		when(dao.read(123)).thenReturn(order);
		when(dao.update(order)).thenReturn(1);
		boolean result = bo.cancelOrder(123);

		assertTrue(result);

		verify(dao).read(123);
		verify(dao).update(order);
	}

}
