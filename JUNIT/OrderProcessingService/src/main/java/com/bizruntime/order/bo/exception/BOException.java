package com.bizruntime.order.bo.exception;

import java.sql.SQLException;

@SuppressWarnings("serial")
public class BOException extends Exception {

	public BOException(SQLException e) {
		super(e);
	}

}
