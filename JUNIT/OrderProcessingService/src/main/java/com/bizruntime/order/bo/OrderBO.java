package com.bizruntime.order.bo;

import com.bizruntime.order.bo.exception.BOException;
import com.bizruntime.order.dto.Order;

public interface OrderBO {

	boolean placeOrder(Order order) throws BOException;

	boolean cancelOrder(int id) throws BOException;

	boolean deleteOrder(int id) throws BOException;

}
