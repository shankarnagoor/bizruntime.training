
package example;

import junit.framework.TestCase;

import org.xml.sax.SAXException;

import com.bizruntime.example.Main;

import java.io.IOException;
import java.util.List;


public class CSVtoJavaTest extends TestCase {

    public void test() throws IOException, SAXException {
        List result = Main.runSmooksTransform();

        assertEquals("[[charles, moulliard, Male, 43, belgium], [maxence, dewil, Male, 30, belgium], [eleonor, moulliard, Female, 12, belgium]]", result.toString());
    }
}
