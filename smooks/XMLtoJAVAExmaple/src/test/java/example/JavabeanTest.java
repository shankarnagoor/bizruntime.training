
package example;

import junit.framework.TestCase;

import java.io.IOException;

import org.xml.sax.SAXException;

import com.bizruntime.example.Main;
import com.bizruntime.example.model.Order;
import com.bizruntime.example.model.OrderItem;


public class JavabeanTest extends TestCase {

    public void test() throws IOException, SAXException {
        Order order = Main.runSmooks();

        assertNotNull(order);
        assertNotNull(order.getHeader());
        assertNotNull(order.getOrderItems());
        assertEquals(2, order.getOrderItems().size());

        assertEquals(1163616328000L, order.getHeader().getDate().getTime());
        assertEquals("Shankar", order.getHeader().getCustomerName());
        assertEquals(new Long(88077), order.getHeader().getCustomerNumber());

        OrderItem orderItem = order.getOrderItems().get(0);
        assertEquals(8.90d, orderItem.getPrice());
        assertEquals(111, orderItem.getProductId());
        assertEquals(new Integer(2), orderItem.getQuantity());

        orderItem = order.getOrderItems().get(1);
        assertEquals(5.20d, orderItem.getPrice());
        assertEquals(222, orderItem.getProductId());
        assertEquals(new Integer(7), orderItem.getQuantity());
    }
}
