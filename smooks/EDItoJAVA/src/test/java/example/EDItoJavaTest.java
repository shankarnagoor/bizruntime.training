
package example;

import junit.framework.*;
import org.xml.sax.*;

import java.io.*;


public class EDItoJavaTest extends TestCase {

    public void test() throws IOException, SAXException {
        String expected = org.milyn.io.StreamUtils.readStreamAsString(getClass().getResourceAsStream("expected.xml"));
        Main smooksMain = new Main();

        org.milyn.payload.JavaResult result = smooksMain.runSmooksTransform();

        com.thoughtworks.xstream.XStream xstream = new com.thoughtworks.xstream.XStream();
        String actual = xstream.toXML(result.getBean("order"));

        actual = actual.replaceFirst("<date>.*</date>", "<date/>");

        boolean matchesExpected = org.milyn.io.StreamUtils.compareCharStreams(new java.io.StringReader(expected), new java.io.StringReader(actual));
        if(!matchesExpected) {
            assertEquals("Actual does not match expected.", expected, actual);
        }
    }
}
