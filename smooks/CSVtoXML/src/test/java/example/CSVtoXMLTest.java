
package example;

import junit.framework.TestCase;

import java.io.IOException;
import java.io.ByteArrayInputStream;

import org.xml.sax.SAXException;
import org.milyn.io.StreamUtils;

public class CSVtoXMLTest extends TestCase {

    public void test() throws IOException, SAXException {
        byte[] expected = StreamUtils.readStream(getClass().getResourceAsStream("expected.xml"));
        String result = Main.runSmooksTransform();

        StringBuffer s1 = StreamUtils.trimLines(new ByteArrayInputStream(expected));
        StringBuffer s2 = StreamUtils.trimLines(new ByteArrayInputStream(result.getBytes()));

        assertEquals("Expected:\n" + s1 + "\nActual:\n" + s2, s1.toString(), s2.toString());
    }
}
