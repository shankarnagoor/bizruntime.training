
package example;

import java.io.IOException;

import junit.framework.TestCase;

import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.milyn.io.StreamUtils;
import org.xml.sax.SAXException;

public class JSONtoJavaTest extends TestCase {

    public void test() throws IOException, SAXException {
        String expected = StreamUtils.readStreamAsString(getClass().getResourceAsStream("expected.xml"));
        Main smooksMain = new Main();
        String result = smooksMain.runSmooksTransform();

        XMLUnit.setIgnoreWhitespace(true);
        XMLAssert.assertXMLEqual(expected, result);
    }
}
