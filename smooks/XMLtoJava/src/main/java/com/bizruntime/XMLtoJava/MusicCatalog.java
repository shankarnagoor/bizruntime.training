package com.bizruntime.XMLtoJava;

import java.util.List;

public class MusicCatalog {
 
 private Header header;
 private List<Album> albumList;
  
 public Header getHeader() {
  return header;
 }
 public void setHeader(Header header) {
  this.header = header;
 }
 public List<Album> getAlbumList() {
  return albumList;
 }
 public void setAlbumList(List<Album> albumList) {
  this.albumList = albumList;
 }
}