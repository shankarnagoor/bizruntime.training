package com.bizruntime.XMLtoJava;


public class Header {
 
    private String date;
    private Long customerNumber;
    private String customerName;
     
 public String getDate() {
  return date;
 }
 public void setDate(String date) {
  this.date = date;
 }
 public Long getCustomerNumber() {
  return customerNumber;
 }
 public void setCustomerNumber(Long customerNumber) {
  this.customerNumber = customerNumber;
 }
 public String getCustomerName() {
  return customerName;
 }
 public void setCustomerName(String customerName) {
  this.customerName = customerName;
 }    
}
 