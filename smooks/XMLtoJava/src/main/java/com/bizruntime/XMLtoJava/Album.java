package com.bizruntime.XMLtoJava;

public class Album { 
	  
	 private String albumTitle;
	 private String artist;
	 private String country;
	 private String company;
	 private Double albumPrice;
	 private Integer releaseYear;
	  
	 public String getAlbumTitle() {
	  return albumTitle;
	 }
	 public void setAlbumTitle(String albumTitle) {
	  this.albumTitle = albumTitle;
	 }
	 public String getArtist() {
	  return artist;
	 }
	 public void setArtist(String artist) {
	  this.artist = artist;
	 }
	 public String getCountry() {
	  return country;
	 }
	 public void setCountry(String country) {
	  this.country = country;
	 }
	 public String getCompany() {
	  return company;
	 }
	 public void setCompany(String company) {
	  this.company = company;
	 }
	 public Double getAlbumPrice() {
	  return albumPrice;
	 }
	 public void setAlbumPrice(Double albumPrice) {
	  this.albumPrice = albumPrice;
	 }
	 public Integer getReleaseYear() {
	  return releaseYear;
	 }
	 public void setReleaseYear(Integer releaseYear) {
	  this.releaseYear = releaseYear;
	 }
	  
	 @Override
	 public String toString() {
	  return "Album [Title=" + albumTitle + ", Artist=" + artist
	    + ", Country=" + country + ", Company=" + company
	    + ", Price=" + albumPrice + ", Year=" + releaseYear
	    + "]";
	 }  
	}