package com.bizruntime.test;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.xml.transform.stream.StreamSource;

import org.milyn.Smooks;
import org.milyn.SmooksException;
import org.milyn.container.ExecutionContext;
import org.milyn.io.StreamUtils;
import org.milyn.payload.JavaResult;
import org.xml.sax.SAXException;

import com.bizruntime.XMLtoJava.Album;
import com.bizruntime.XMLtoJava.MusicCatalog;
 
public class Main {
 
 private static byte[] messageIn = readInputMessage(); 
 
 public static void main(String[] args) throws IOException, SAXException, SmooksException {
 
  MusicCatalog catalog = Main.populateBean();
  System.out.println("Date:"+catalog.getHeader().getDate());
  System.out.println("Customer Name:"+catalog.getHeader().getCustomerName());
  System.out.println("Customer Number:"+catalog.getHeader().getCustomerNumber());
  System.out.println("\nAlbums\n..........");
  List<Album> albumList = catalog.getAlbumList();
 
  for (int i = 0; i < albumList.size(); i++) {
   Album album = albumList.get(i);
   System.out.println((i + 1)+": "+album);     
  }
 }
 
 private static byte[] readInputMessage() {
  try {
   return StreamUtils.readStream(new FileInputStream("Input.xml"));
  } catch (IOException e) {
   e.printStackTrace();
   return "<no-message/>".getBytes();
  }
 }
  
 protected static MusicCatalog populateBean() throws IOException, SAXException, SmooksException {
 
  Smooks smooks = new Smooks("smooks-config.xml");
  try {
   ExecutionContext executionContext = smooks.createExecutionContext();
   JavaResult result = new JavaResult();
   ByteArrayInputStream msgStream = new ByteArrayInputStream(messageIn);
   smooks.filterSource(new StreamSource(msgStream), result);
   return (MusicCatalog) result.getBean("catalog");
 
  } finally {
   smooks.close();
  }
 }
}