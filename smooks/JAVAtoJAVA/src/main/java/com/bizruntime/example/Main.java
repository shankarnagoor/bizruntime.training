
package com.bizruntime.example;

import org.milyn.*;
import org.milyn.container.*;
import org.milyn.event.report.*;
import org.milyn.payload.*;
import org.xml.sax.*;

import com.bizruntime.example.srcmodel.*;
import com.bizruntime.example.trgmodel.*;

import java.io.*;


public class Main {

   
    public LineOrder runSmooksTransform(Order srcOrder) throws IOException, SAXException {
        Smooks smooks = new Smooks("smooks-config.xml");

        try {
            ExecutionContext executionContext = smooks.createExecutionContext();

             JavaSource source = new JavaSource(srcOrder);
            JavaResult result = new JavaResult();

            // Configure the execution context to generate a report...
            executionContext.setEventListener(new HtmlReportGenerator("target/report/report.html"));

            smooks.filterSource(executionContext, source, result);

            return (LineOrder) result.getBean("lineOrder");
        } finally {
            smooks.close();
        }
    }

    public static void main(String[] args) throws IOException, SAXException, SmooksException {
        Main smooksMain = new Main();
        Order order = new Order();
        LineOrder lineOrder;

        pause("Press 'enter' to display the input Java Order message...");
        System.out.println("\n");
        System.out.println(order);
        System.out.println("\n\n");

        System.out.println("This needs to be transformed to another Java Object.");
        pause("Press 'enter' to display the transformed Java Object...");
        lineOrder = smooksMain.runSmooksTransform(order);
        System.out.println("\n");
        System.out.println(lineOrder);
        System.out.println("\n\n");

        pause("And that's it!");
        System.out.println("\n\n");
    }

    private static void pause(String message) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("> " + message);
            in.readLine();
        } catch (IOException e) {
        }
        System.out.println("\n");
    }
}