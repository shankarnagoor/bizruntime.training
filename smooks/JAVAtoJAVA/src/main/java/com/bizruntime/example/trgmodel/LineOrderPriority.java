
package com.bizruntime.example.trgmodel;


public enum LineOrderPriority 
 {

	NOT_IMPORTANT,
	IMPORTANT,
	VERY_IMPORTANT;

  }
