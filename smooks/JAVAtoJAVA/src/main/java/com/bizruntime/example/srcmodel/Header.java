
package com.bizruntime.example.srcmodel;
public class Header {

    private Long customerNumber = 1234L;

    private String customerName = "Buzz Lightyear";

    private Priority priority = Priority.HIGH;

    public Long getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(Long customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

	/**
	 * @return the priority
	 */
	public Priority getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(Priority priority) {
		this.priority = priority;
	}

    @Override
	public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("customerNumber: " + customerNumber + ", ");
        stringBuilder.append("customerName: " + customerName + ", ");
        stringBuilder.append("priority: " + priority);


        return stringBuilder.toString();
    }

}