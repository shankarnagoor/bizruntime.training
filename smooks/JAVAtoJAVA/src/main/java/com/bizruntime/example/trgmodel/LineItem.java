
package com.bizruntime.example.trgmodel;

import java.math.BigDecimal;


public class LineItem {
    private String productCode;
    private int unitQuantity;
    private BigDecimal unitPrice;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getUnitQuantity() {
        return unitQuantity;
    }

    public void setUnitQuantity(int unitQuantity) {
        this.unitQuantity = unitQuantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("{productCode: " + productCode + " | ");
        stringBuilder.append("unitQuantity: " + unitQuantity + " | ");
        stringBuilder.append("unitPrice: " + unitPrice + "}");

        return stringBuilder.toString();
    }
}