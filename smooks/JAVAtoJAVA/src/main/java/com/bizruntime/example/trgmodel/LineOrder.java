
package com.bizruntime.example.trgmodel;

import java.util.Arrays;

public class LineOrder {
    private String customerId;
    private String customerName;

    private LineOrderPriority priority;

    private LineItem[] lineItems;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public LineItem[] getLineItems() {
        return lineItems;
    }

    public void setLineItems(LineItem[] lineItems) {
        this.lineItems = lineItems;
    }

	/**
	 * @return the priority
	 */
	public LineOrderPriority getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(LineOrderPriority priority) {
		this.priority = priority;
	}

    @Override
	public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Class: " + getClass().getName() + "\n");
        stringBuilder.append("\tcustomerId: " + customerId + "\n");
        stringBuilder.append("\tcustomerName: " + customerName + "\n");
        stringBuilder.append("\tpriority: " + getPriority() + "\n");
        if(lineItems != null) {
            stringBuilder.append("\tlineItems: " + Arrays.asList(lineItems));
        }

        return stringBuilder.toString();
    }


}