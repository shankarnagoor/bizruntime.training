package admin;
import accounts.Account;
import accounts.SavingsAccount;
import accounts.LoanAccount;

public class Manager {

public Account openAccount(char type,String name,double initamt){
	Account acc1= null;
	   if (type == 's'){
		   System.out.println("creating savings account");
		   acc1 = new SavingsAccount(123469,"vishal",initamt);
	   }else if (type == 'l'){
		   System.out.println("creating loan account");
		   acc1 = new LoanAccount(123469,"shank",initamt);
	   }
	   
		 return acc1;  
	   }
   
}
