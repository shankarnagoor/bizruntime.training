package deposit;

public class Customer {
       private String name;
       private long phNum;
       
       public Customer(String name,long phNum)
       {
    	   this.name=name;
    	   this.phNum=phNum;
       }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPhNum() {
		return phNum;
	}

	public void setPhNum(long phNum) {
		this.phNum = phNum;
	}
      
}
