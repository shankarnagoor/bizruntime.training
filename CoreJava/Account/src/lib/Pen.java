package lib;

public class Pen {
	String inkColor;
	double price;

	public Pen(String inkColor, double price){
		this.inkColor = inkColor;
		this.price = price;
	}

	public void write() 
	{
		System.out.println("writing with inkcolor:"+this.inkColor );
		System.out.println("price of the pen is:"+this.price);
		
	}

    
public String toString()
{
 return "Pen[color:"+this.inkColor +",Price:"+this.price +"]";
  }
 }
 
