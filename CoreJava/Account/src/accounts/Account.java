package accounts;

public interface Account {

	void checkBalance();
	void deposit(double amt);
	void withDrawl(double amt);
}
