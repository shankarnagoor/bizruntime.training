package accounts;

public class LoanAccount implements Account{
	int accNum;
	String custName;
	double accBal;

	public LoanAccount(int accNum,String custName,double accBal){
		this.accNum = accNum;
		this.custName = custName;
		this.accBal = accBal;
	}
	public void checkBalance(){
		System.out.println("Account balance:Rs"+accBal);
	}
	
	public void deposit(double amt){
		System.out.println("Depositing Rs:" +amt);
		accBal=accBal-amt;
	}
	public void withDrawl(double amt){
		System.out.println("Withdrawing Rs:"+amt);
		accBal=accBal+amt;
	}
}


